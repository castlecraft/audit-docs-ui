FROM node:latest
ARG STAGE=production
# Copy and build server
COPY . /opt/audit-docs-ui
WORKDIR /opt/
RUN cd audit-docs-ui \
    && npm install \
    && npm run build -- --configuration ${STAGE} \
    && npx cap sync && npx cap copy

FROM nginxinc/nginx-unprivileged:latest

COPY --from=0 /opt/audit-docs-ui/www /var/www/html
COPY ./docker/nginx.conf /etc/nginx/conf.d/default.conf
