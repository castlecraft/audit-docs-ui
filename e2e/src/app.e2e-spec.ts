import { AppPage } from './app.po';

describe('new App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });
  describe('default screen', () => {
    it('should load home', () => {
      expect(page).toBeDefined();
    });
  });
});
