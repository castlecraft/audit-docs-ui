import { Component, OnInit } from '@angular/core';
import { TokenService } from '../auth/token/token.service';
import { switchMap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { LOGGED_IN } from '../auth/token/constants';
import { SET_ITEM, StorageService } from '../auth/storage/storage.service';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Color, Label, MultiDataSet } from 'ng2-charts';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  name: string;
  email: string;
  picture: string;
  loggedIn: boolean;
  accessToken: string;
  phone: string;

  public lineChartData: ChartDataSets[] = [
    {
      data: [65, 119, 80, 81, 56, 55, 40],
      label: 'Series A',
      fill: false,
    },
  ];
  public lineChartLabels: Label[] = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
  ];
  public chartOptions: ChartOptions = {};
  public lineChartColors: Color[] = [
    {
      borderColor: 'rgb(75, 192, 192)',
    },
  ];
  public lineChartLegend = true;
  public lineChartType: ChartType = 'line';
  public pieChartType: ChartType = 'pie';
  public lineChartPlugins = [];

  // DOUGHNUT
  public doughnutChartLabels: Label[] = [
    'Supplies',
    'Professional Expense',
    'Total Expense',
  ];
  public doughnutChartData: MultiDataSet = [[30, 6, 30]];
  public doughnutChartType: ChartType = 'doughnut';

  // BAR CHART
  public barChartLabels: Label[] = ['Expense', 'Income'];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartPlugins = [];

  public barChartData: ChartDataSets[] = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Expense' },
    { data: [28, 48, 40, 19, 86, 27, 90], label: 'Income' },
  ];

  constructor(
    private readonly token: TokenService,
    private readonly http: HttpClient,
    private readonly store: StorageService,
  ) {}

  ngOnInit() {
    this.store.getItem(LOGGED_IN).then(loggedIn => {
      this.loggedIn = loggedIn === 'true';
    });

    this.store.changes.subscribe({
      next: res => {
        if (res.event === SET_ITEM && res.value?.key === LOGGED_IN) {
          this.loggedIn = res.value?.value === 'true';
          if (this.loggedIn) {
            this.loadProfile();
          }
        }
      },
      error: error => {},
    });

    this.token.config
      .pipe(
        switchMap(config => {
          return this.token.getToken().pipe(
            switchMap(token => {
              this.accessToken = token;
              return this.http.get<any>(config.profileUrl, {
                headers: { authorization: 'Bearer ' + token },
              });
            }),
          );
        }),
      )
      .subscribe({
        next: profile => {
          this.name = profile.name;
          this.email = profile.email;
          this.picture = profile.picture;
        },
        error: error => {},
      });
  }

  loadProfile() {
    this.token.config
      .pipe(
        switchMap(config => {
          return this.token.getToken().pipe(
            switchMap(token => {
              this.accessToken = token;
              return this.http.get<any>(config.profileUrl, {
                headers: { authorization: 'Bearer ' + token },
              });
            }),
          );
        }),
      )
      .subscribe({
        next: profile => {
          this.name = profile.name;
          this.email = profile.email;
          this.picture = profile.picture;
          this.phone = profile.phone_number;
        },
        error: error => {},
      });
  }
}
