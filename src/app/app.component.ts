import { Component, NgZone } from '@angular/core';
import { App } from '@capacitor/app';
import { Router } from '@angular/router';

import { SET_ITEM, StorageService } from './auth/storage/storage.service';
import { LOGGED_IN } from './auth/token/constants';
import { TokenService } from './auth/token/token.service';
import { environment } from '../environments/environment';
import { DURATION } from './constants/app-strings';
import { switchMap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  loggedIn: boolean;
  cap = App;
  fullName: string = '';
  imageURL: string = '';

  constructor(
    private readonly token: TokenService,
    private readonly store: StorageService,
    private readonly router: Router,
    private readonly zone: NgZone,
    private readonly http: HttpClient,
  ) {
    this.initializeApp();
  }

  ngOnInit() {
    this.store.getItem(LOGGED_IN).then(loggedIn => {
      this.loggedIn = loggedIn === 'true';
    });
    this.store.changes.subscribe({
      next: res => {
        if (res.event === SET_ITEM && res.value?.key === LOGGED_IN) {
          this.loggedIn = res.value?.value === 'true';
        }
      },
      error: error => {},
    });
    setTimeout(() => {
      if (!this.loggedIn) {
        this.logIn();
      }
    }, DURATION);

    this.token.config
      .pipe(
        switchMap(config => {
          return this.token.getToken().pipe(
            switchMap(token => {
              return this.http.get<any>(config.profileUrl, {
                headers: { authorization: 'Bearer ' + token },
              });
            }),
          );
        }),
      )
      .subscribe({
        next: profile => {
          this.fullName = profile.name;
          this.imageURL = profile.picture;
        },
        error: error => {},
      });
  }

  initializeApp() {
    this.cap.addListener('appUrlOpen', (data: any) => {
      this.zone.run(() => {
        const slug = data.url.split(`${environment.callbackProtocol}://`).pop();
        if (slug) {
          this.router.navigateByUrl(slug);
        }
      });
    });
  }

  logIn() {
    this.token.logIn();
  }

  logOut() {
    this.token.logOut();
  }
}
