import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { NavigationEnd, Router, RouterEvent } from '@angular/router';
import { ListingService } from '../shared/data-list/listing.service';
import { SalesInvoiceDataSource } from './sales-invoice-datasource';

@Component({
  selector: 'app-sales',
  templateUrl: './sales.page.html',
  styleUrls: ['./sales.page.scss'],
})
export class SalesPage implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('FileSelectInputDialog') fileSelectInputDialog: ElementRef;

  dataSource: SalesInvoiceDataSource;
  displayedColumns = ['name', 'status', 'company', 'customer'];

  constructor(private listingService: ListingService, private router: Router) {}

  ngOnInit() {
    this.dataSource = new SalesInvoiceDataSource(this.listingService);
    this.dataSource.loadItems();
    this.router.events.subscribe({
      next: (event: RouterEvent) => {
        if (event instanceof NavigationEnd) {
          this.dataSource.loadItems();
        }
      },
      error: err => {},
    });
  }

  getUpdate(event) {
    this.paginator.pageIndex = event?.pageIndex || 0;
    this.paginator.pageSize = event?.pageSize || 30;

    this.dataSource.loadItems(
      undefined,
      undefined,
      event?.pageIndex || undefined,
      event?.pageSize || undefined,
    );
  }

  openAddPdfDialog() {
    const e: HTMLElement = this.fileSelectInputDialog.nativeElement;
    e.click();
  }
}
