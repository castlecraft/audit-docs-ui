import { NgModule } from '@angular/core';

import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';

import { SalesPage } from './sales.page';
import { SharedModule } from '../shared/shared.module';

const routes: Routes = [
  {
    path: '',
    component: SalesPage,
  },
];

@NgModule({
  imports: [IonicModule, SharedModule, RouterModule.forChild(routes)],
  declarations: [SalesPage],
})
export class SalesPageModule {}
