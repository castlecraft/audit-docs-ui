import { DataSource } from '@angular/cdk/table';
import { BehaviorSubject, of } from 'rxjs';
import { catchError, finalize, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { SalesInvoice } from '../common/interfaces/sales.interface';
import { SALES_INVOICE } from '../constants/url-strings';
import { ListingService } from '../shared/data-list/listing.service';

export class SalesInvoiceDataSource extends DataSource<SalesInvoice> {
  length: number;
  offset: number;
  itemSubject = new BehaviorSubject<SalesInvoice[]>([]);
  loadingSubject = new BehaviorSubject<boolean>(false);

  loading$ = this.loadingSubject.asObservable();

  constructor(private readonly listingService: ListingService) {
    super();
  }

  connect() {
    return this.itemSubject.asObservable();
  }
  disconnect() {
    this.itemSubject.complete();
    this.loadingSubject.complete();
  }

  loadItems(
    filter = [],
    sortOrder = 'modified asc',
    pageIndex = 0,
    pageSize = 30,
  ) {
    this.loadingSubject.next(true);
    const url = environment.authServerURL + SALES_INVOICE;
    this.listingService
      .findModels(url, filter, sortOrder, pageIndex, pageSize)
      .pipe(
        map(res => {
          this.data = res.docs;
          this.offset = res.offset;
          this.length = res.length;
          return res.docs;
        }),
        catchError(() => of([])),
        finalize(() => this.loadingSubject.next(false)),
      )
      .subscribe(items => {
        this.itemSubject.next(items);
      });
  }

  data() {
    return this.itemSubject.value;
  }

  update(data) {
    this.itemSubject.next(data);
  }
}
