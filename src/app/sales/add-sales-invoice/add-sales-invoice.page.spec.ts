import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { IonicModule } from '@ionic/angular';
import { of } from 'rxjs';
import { MaterialModule } from '../../material.module';
import { ListingService } from '../../shared/data-list/listing.service';

import { AddSalesInvoicePage } from './add-sales-invoice.page';

describe('AddSalesInvoicePage', () => {
  let component: AddSalesInvoicePage;
  let fixture: ComponentFixture<AddSalesInvoicePage>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [AddSalesInvoicePage],
        imports: [
          IonicModule.forRoot(),
          RouterTestingModule.withRoutes([]),
          MaterialModule,
          NoopAnimationsModule,
        ],
        providers: [
          {
            provide: ListingService,
            useValue: {
              findModels: (...args) => of({ docs: [] }),
            } as ListingService,
          },
        ],
      }).compileComponents();

      fixture = TestBed.createComponent(AddSalesInvoicePage);
      component = fixture.componentInstance;
      fixture.detectChanges();
    }),
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
