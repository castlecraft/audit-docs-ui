import { NgModule } from '@angular/core';

import { IonicModule } from '@ionic/angular';

import { AddSalesInvoicePageRoutingModule } from './add-sales-invoice-routing.module';

import { AddSalesInvoicePage } from './add-sales-invoice.page';
import { InlineEditComponent } from './inline-edit/inline-edit.component';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [IonicModule, AddSalesInvoicePageRoutingModule, SharedModule],
  declarations: [AddSalesInvoicePage, InlineEditComponent],
  exports: [InlineEditComponent],
})
export class AddSalesInvoicePageModule {}
