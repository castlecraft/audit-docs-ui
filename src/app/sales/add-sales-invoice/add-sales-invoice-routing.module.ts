import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddSalesInvoicePage } from './add-sales-invoice.page';

const routes: Routes = [
  {
    path: '',
    component: AddSalesInvoicePage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddSalesInvoicePageRoutingModule {}
