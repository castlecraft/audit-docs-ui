import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { map, startWith, switchMap } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { Item, SalesInvoice } from '../../common/interfaces/sales.interface';
import {
  CREATE_MESSAGE,
  DURATION,
  SUBMIT_MESSAGE,
  SYNCED_MESSAGE,
  UNIDENTIFIED_ERROR_MESSAGE,
  UPDATE_MESSAGE,
} from '../../constants/app-strings';
import {
  COMPANY,
  SALES_INVOICE,
  SYNC_DOCUMENT,
} from '../../constants/url-strings';
import { ListingService } from '../../shared/data-list/listing.service';
import { ItemsDataSource } from './items-datasource';

@Component({
  selector: 'app-add-sales-invoice',
  templateUrl: './add-sales-invoice.page.html',
  styleUrls: ['./add-sales-invoice.page.scss'],
})
export class AddSalesInvoicePage implements OnInit {
  addSalesInvoiceForm: FormGroup;
  itemsControl: FormArray;
  dataSource: ItemsDataSource;
  displayedColumns = ['item', 'quantity', 'rate', 'total', 'delete'];
  uuid = '';
  docstatus: number = 0;
  title = '';
  filteredCompany: Observable<any>;
  isSynced: number = 0;

  get f() {
    return this.addSalesInvoiceForm.controls;
  }

  constructor(
    private readonly location: Location,
    private readonly service: ListingService,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    public toastController: ToastController,
  ) {}

  ngOnInit() {
    this.createFormGroup();
    this.uuid = this.route.snapshot.params.uuid;
    if (this.uuid) {
      this.populateForm();
    }

    this.filteredCompany = this.addSalesInvoiceForm
      .get('company')
      .valueChanges.pipe(
        startWith(''),
        switchMap(value =>
          this.service
            .findModels(environment.frappeUrl + COMPANY, [
              ['company_name', 'like', `%${value}%`],
            ])
            .pipe(map(res => res.docs)),
        ),
      );

    this.dataSource = new ItemsDataSource();
    this.addSalesInvoiceForm.get('postingDate').setValue(new Date());
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message,
      duration: DURATION,
    });
    toast.present();
  }

  createFormGroup() {
    this.addSalesInvoiceForm = new FormGroup({
      company: new FormControl('', [Validators.required]),
      customer: new FormControl('', [Validators.required]),
      postingDate: new FormControl('', [Validators.required]),
      remarks: new FormControl(''),
      items: new FormArray([]),
      total: new FormControl(0),
    });
    this.itemsControl = this.addSalesInvoiceForm.get('items') as FormArray;
  }

  navigateBack() {
    this.location.back();
  }

  createInvoice() {
    const salesInvoice = {} as SalesInvoice;
    salesInvoice.company = this.addSalesInvoiceForm.get('company').value;
    salesInvoice.customer = this.addSalesInvoiceForm.get('customer').value;
    salesInvoice.posting_date = this.getParsedDate(
      this.addSalesInvoiceForm.get('postingDate').value,
    );
    salesInvoice.remarks = this.addSalesInvoiceForm.get('remarks').value;
    salesInvoice.total = this.addSalesInvoiceForm.get('total').value;
    salesInvoice.item = this.dataSource.data();

    return this.service
      .postRequest(environment.frappeUrl + SALES_INVOICE, salesInvoice)
      .subscribe({
        next: success => {
          this.presentToast(CREATE_MESSAGE);
          const response = success.data.name;
          this.router.navigate(['sales/update-sales-invoice/' + response]);
        },
      });
  }

  updateInvoice() {
    const salesInvoice = {} as SalesInvoice;
    salesInvoice.company = this.addSalesInvoiceForm.get('company').value;
    salesInvoice.customer = this.addSalesInvoiceForm.get('customer').value;
    salesInvoice.posting_date = this.getParsedDate(
      this.addSalesInvoiceForm.get('postingDate').value,
    );
    salesInvoice.remarks = this.addSalesInvoiceForm.get('remarks').value;
    salesInvoice.total = this.addSalesInvoiceForm.get('total').value;
    salesInvoice.item = this.dataSource.data();

    this.service
      .putRequest(
        environment.frappeUrl + SALES_INVOICE + '/' + this.uuid,
        salesInvoice,
      )
      .subscribe({
        next: res => {
          this.presentToast(UPDATE_MESSAGE);
        },
        error: err => {
          this.presentToast(UNIDENTIFIED_ERROR_MESSAGE);
        },
      });
  }

  submitInvoice() {
    this.service
      .putRequest(
        environment.authServerURL +
          '/api/resource/Preva%20Sales%20Invoice/' +
          this.uuid,
        { docstatus: 1 },
      )
      .subscribe({
        next: res => {
          this.docstatus = 1;
        },
        error: error => {
          this.presentToast(UNIDENTIFIED_ERROR_MESSAGE);
        },
      });
  }

  sync() {
    this.service
      .getRequest(environment.frappeUrl + SYNC_DOCUMENT, {
        invoice: this.uuid,
      })
      .subscribe({
        next: success => {
          this.isSynced = 1;
          this.presentToast(SYNCED_MESSAGE);
          this.router.navigate(['/sales']);
        },
        error: err => {
          this.presentToast(UNIDENTIFIED_ERROR_MESSAGE);
        },
      });
  }

  submit() {
    this.service
      .putRequest(environment.frappeUrl + SALES_INVOICE + '/' + this.uuid, {
        docstatus: 1,
      })
      .subscribe({
        next: success => {
          this.presentToast(SUBMIT_MESSAGE);
          this.docstatus = success.data.docstatus;
          this.router.navigate(['/sales']);
        },
        error: err => {
          this.presentToast(UNIDENTIFIED_ERROR_MESSAGE);
        },
      });
  }

  getParsedDate(value) {
    const date = new Date(value);
    return [
      date.getFullYear(),
      date.getMonth() + 1,
      // +1 as index of months start's from 0
      date.getDate(),
    ].join('-');
  }

  addItem() {
    const data = this.dataSource.data();
    const item = {} as Item;
    item.item_name = '';
    item.quantity = 0;
    item.rate = 0;
    data.push(item);
    this.itemsControl.push(new FormControl(item));
    this.dataSource.update(data);
  }

  updateField(row: Item, index: number, item: Item) {
    if (item == null) {
      return;
    }
    Object.assign(row, item);
    if (item.item_name) {
      row.item_name = item.item_name;
    }
    if (item.quantity) {
      row.quantity = item.quantity;
    }
    if (item.rate) {
      row.rate = item.rate;
    }
    if (item.total) {
      row.total = item.total;
    }

    const copy = this.dataSource.data().slice();
    this.calculateTotal(this.dataSource.data().slice());

    this.dataSource.update(copy);
    this.itemsControl.controls[index].setValue(item);
  }

  calculateTotal(itemList: Item[]) {
    let sum = 0;
    itemList.forEach(item => {
      sum += item.quantity * item.rate;
    });
    this.addSalesInvoiceForm.get('total').setValue(sum);
  }

  deleteRow(i: number) {
    this.dataSource.data().splice(i, 1);
    this.itemsControl.removeAt(i);
    this.calculateTotal(this.dataSource.data().slice());
    this.dataSource.update(this.dataSource.data());
  }

  populateForm() {
    this.service
      .getRequest(environment.frappeUrl + SALES_INVOICE + '/' + this.uuid)
      .subscribe({
        next: success => {
          this.title = success?.data?.erpnext_name;
          this.docstatus = success?.data?.docstatus || 0;
          this.f.company.setValue(success.data.company);
          this.f.customer.setValue(success.data.customer);
          this.f.postingDate.setValue(success.data.posting_date);
          this.f.remarks.setValue(success.data.remarks);
          this.f.total.setValue(success.data.total);
          this.dataSource.update(success.data.item);
          this.isSynced = success?.data?.is_synced;
          if (this.docstatus) {
            this.addSalesInvoiceForm.disable();
          }
        },
      });
  }

  print() {
    const authURL = environment.authServerURL;
    const url =
      authURL +
      '/api/method/frappe.utils.print_format.download_pdf?doctype=Preva%20Sales%20Invoice&name=' +
      this.uuid;
    window.open(url, '_blank');
  }
}
