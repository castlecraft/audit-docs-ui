import { Component, Input, Optional, Host } from '@angular/core';
import { SatPopover } from '@ncstate/sat-popover';
import { FormControl } from '@angular/forms';
import { Item } from '../../../common/interfaces/sales.interface';

@Component({
  selector: 'inline-edit',
  templateUrl: './inline-edit.component.html',
  styleUrls: ['inline-edit.component.scss'],
})
export class InlineEditComponent {
  @Input()
  get value(): any {
    return this._value;
  }
  set value(x) {
    this._value = x;
  }

  @Input()
  column: string;

  private _value = '';

  item = new FormControl();
  rate = new FormControl();
  quantity = new FormControl();

  constructor(@Optional() @Host() public popover: SatPopover) {}

  ngOnInit() {}

  getOptionText(option) {
    return option.item_name;
  }

  onSubmit() {
    if (this.popover) {
      const item = {} as Item;
      if (this.column === 'item') {
        item.item_name = this.item.value;
      }
      if (this.column === 'quantity') {
        item.quantity = this.quantity.value;
      }
      if (this.column === 'rate') {
        item.rate = this.rate.value;
      }
      this.popover.close(item);
    }
  }

  onCancel() {
    if (this.popover) {
      this.popover.close();
    }
  }
}
