export const SALES_INVOICE = '/api/resource/Preva Sales Invoice';
export const COMPANY = '/api/resource/Company';
export const SYNC_DOCUMENT = '/api/method/audit_documents.api.sync_sales_inv';
export const WEB_LOGOUT = '?cmd=web_logout';
