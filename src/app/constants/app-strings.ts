export const DURATION = 2000;
export const UPDATE_MESSAGE = 'Updated successfully.';
export const CREATE_MESSAGE = 'Created successfully.';
export const SYNCED_MESSAGE = 'Synced successfully.';
export const SUBMIT_MESSAGE = 'Submitted successfully.';
export const UNIDENTIFIED_ERROR_MESSAGE = 'Something went wrong.';
