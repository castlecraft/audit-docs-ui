export class Item {
  uuid?: string;
  item_name?: string;
  quantity?: number;
  rate?: number;
  total?: number;
}

export class SalesInvoice {
  uuid?: string;
  company?: string;
  customer: string;
  item?: Item[];
  posting_date: string;
  remarks: string;
  total?: number;
}
