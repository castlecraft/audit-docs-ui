import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import { MatPaginator } from '@angular/material/paginator';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { catchError, finalize, map } from 'rxjs/operators';
import { ListingData } from '../interfaces/listing-data.interface';
import { ListResponse } from '../interfaces/listing-response.interface';
import { ListingService } from './listing.service';

export class ListingDataSource extends DataSource<ListingData> {
  data: ListingData[] = [];
  length: number = 0;
  offset: number = 0;
  itemSubject = new BehaviorSubject<ListingData[]>([]);
  loadingSubject = new BehaviorSubject<boolean>(false);
  paginator?: MatPaginator;

  loading$ = this.loadingSubject.asObservable();

  constructor(
    public endpoint: string,
    private readonly listingService: ListingService,
    private _headers: any = {},
  ) {
    super();
  }

  connect(collectionViewer: CollectionViewer): Observable<ListingData[]> {
    return this.itemSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this.itemSubject.complete();
    this.loadingSubject.complete();
  }

  loadItems(filter = [], sortOrder = undefined, pageIndex = 0, pageSize = 10) {
    this.loadingSubject.next(true);
    this.listingService
      .findModels(
        this.endpoint,
        filter,
        sortOrder,
        pageIndex,
        pageSize,
        this._headers,
      )
      .pipe(
        map((res: ListResponse) => {
          this.data = res.docs;
          this.offset = res.offset;
          this.length = res.length;
          return res.docs;
        }),
        catchError(() => of([])),
        finalize(() => this.loadingSubject.next(false)),
      )
      .subscribe(items => this.itemSubject.next(items));
  }

  set headers(headers: any) {
    this._headers = headers;
  }
}
