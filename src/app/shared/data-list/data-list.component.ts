import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ListingDataSource } from './data-list.datasource';
import { ListingService } from './listing.service';

@Component({
  selector: 'app-data-list',
  templateUrl: './data-list.component.html',
  styleUrls: ['./data-list.component.scss'],
})
export class DataListComponent implements OnInit {
  @Input() filters = [];
  @Input() headers = {};
  @Input() endpoint = '';
  @Input() fields = ['uuid'];
  @Input() linkCol = 'uuid';
  @Input() linkPrefix = '';
  @ViewChild(MatPaginator, { static: true }) paginator?: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort?: MatSort;
  @Output() clickRow = new EventEmitter();
  dataSource = new ListingDataSource(
    this.endpoint,
    this.listingService,
    this.headers,
  );

  constructor(private readonly listingService: ListingService) {}

  ngOnInit() {
    this.listingService.changes.subscribe(() => this.getUpdate());

    this.dataSource = new ListingDataSource(
      this.endpoint,
      this.listingService,
      this.headers,
    );
    this.dataSource.loadItems(this.filters);
  }

  getUpdate(event?: { pageIndex: number; pageSize: number }) {
    const sortOrder =
      this.sort?.active && this.sort?.direction
        ? `${this.sort?.active} ${this.sort?.direction}`
        : undefined;

    this.dataSource?.loadItems(
      this.filters,
      sortOrder,
      event?.pageIndex || this.paginator?.pageIndex,
      event?.pageSize || this.paginator?.pageSize,
    );
  }

  snakeToTitleCase(str: string) {
    if (!str) return;
    const result = str.replace(/([A-Z])/g, ' $1');
    return result.charAt(0).toUpperCase() + result.slice(1);
  }

  kebabCase(str: string) {
    if (!str) return;

    return str
      .replace(/([a-z])([A-Z])/g, '$1-$2')
      .replace(/\s+/g, '-')
      .toLowerCase();
  }

  handleRowClick(row) {
    this.clickRow.emit(row);
  }
}
