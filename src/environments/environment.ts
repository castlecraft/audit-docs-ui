// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  // Configure Variables
  frappeUrl: 'https://audit-docs-api.castlecraft.in',
  appURL: 'http://localhost:4200',
  authorizationURL:
    'https://audit-docs-api.castlecraft.in/api/method/frappe.integrations.oauth2.authorize',
  authServerURL: 'https://audit-docs-api.castlecraft.in',
  callbackProtocol: 'frappe',
  callbackUrl: 'http://localhost:4200/callback',
  clientId: 'e780b90cd9',
  profileURL:
    'https://audit-docs-api.castlecraft.in/api/method/frappe.integrations.oauth2.openid_profile',
  revocationURL:
    'https://audit-docs-api.castlecraft.in/api/method/frappe.integrations.oauth2.revoke_token',
  scope: 'openid all',
  tokenURL:
    'https://audit-docs-api.castlecraft.in/api/method/frappe.integrations.oauth2.get_token',
  isAuthRequiredToRevoke: false,
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
