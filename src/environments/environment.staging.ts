// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,

  // Configure Variables
  frappeUrl: 'https://audit-docs-api.castlecraft.in',
  appURL: 'https://preva.castlecraft.in',
  authorizationURL:
    'https://audit-docs-api.castlecraft.in/api/method/frappe.integrations.oauth2.authorize',
  authServerURL: 'https://audit-docs-api.castlecraft.in',
  callbackProtocol: 'frappe',
  callbackUrl: 'https://preva.castlecraft.in/callback',
  clientId: 'e780b90cd9',
  profileURL:
    'https://audit-docs-api.castlecraft.in/api/method/frappe.integrations.oauth2.openid_profile',
  revocationURL:
    'https://audit-docs-api.castlecraft.in/api/method/frappe.integrations.oauth2.revoke_token',
  scope: 'openid all',
  tokenURL:
    'https://audit-docs-api.castlecraft.in/api/method/frappe.integrations.oauth2.get_token',
  isAuthRequiredToRevoke: false,
};
